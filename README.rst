BCFTools Split VCF GeneFlow App
===============================

Version: 1.9-01

This GeneFlow app wraps the BCFTools query tool to split a multi-sample VCF file into individual VCF files.

Inputs
------

1. input: VCF file with one or more samples.

Parameters
----------

1. output: Directory that contains a output VCF files.

